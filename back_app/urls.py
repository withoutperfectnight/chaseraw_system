from django.urls import path
from . import views
urlpatterns = [
    path('mylogin', views.mylogin),
    path('myregister/', views.myregister),
    # path('table', views.table, name="get_table"),
    # path('table/get_table/', views.get_table, name="table"),

    # test _ajax
    # path('ajax_get/', views.ajax_get),
    # path('ajax_post/', views.ajax_post),


    # test _yzp
    # path('index/', views.index),
    # path('provider.html/',views.index5),
    # path('provider_return_table/',views.return_table_provider), # 响应前端请求 给表格数据
    # path('provider/get_table/',views.get_table_provider),#响应前端请求 给表格数据


    # warehouser
    path('windex/',views.windex),
    path('whistory/',views.whistory),
    path('wnumber/',views.wnumber),
    path('wnumber/get_table/',views.wnumber_get_table),#响应前端请求 给表格数据
    path('wnumber/return_table/',views.wnumber_return_table), # 响应修改
    path('wraw/',views.wraw),
    path('wraw/get_table/',views.wraw_get_table),#响应前端请求 给表格数据
    path('whouse/', views.whouse),
    path('whouse/get_table/', views.whouse_get_table),  # 响应前端请求 给表格数据


    # salesman
    path('sindex/', views.sindex),

    path('sfile/', views.sfile),
    path('sfile/get_table/', views.sfile_get_table),  # 响应前端请求 给表格数据
    path('sfile/return_table/', views.sfile_return_table),  # 响应修改

    path('stodo/', views.stodo),
    path('scenter/', views.scenter),

    path('sprovider/', views.sprovider),
    path('sprovider/get_table/', views.sprovider_get_table),  # 响应前端请求 给表格数据

    # t
    path('tindex/', views.tindex),
    path('tfile/', views.tfile),
    path('tfile/get_table/', views.tfile_get_table),  # 响应前端请求 给表格数据
    path('tfile/return_table/', views.tfile_return_table),  # 响应修改

    path('traw/', views.traw),

    path('tsale/', views.tsale),
    path('tsale/get_table/', views.tsale_get_table),  # 响应前端请求 给表格数据

    path('tinventory/', views.tinventory),
    path('tinventory/get_table/', views.tinventory_get_table),  # 响应前端请求 给表格数据
    path('tinventory/return_table/', views.tinventory_return_table),  # 响应修改

    # # 用于测试的url
    # path('test/',views.test),
    # path('test/get_test_data/',views.get_test_data),
    # path('test/error',views.error),
    # # 用于测试的url
    #
    # # 登录模块
    # path('login/',views.gotologin),
    # path('gotoadduser/', views.gotoadduser),
    # path('adduser/', views.addUser),
    # path('logoutapi/',views.logoutapi),
    # path('gotoaddsuperuser/',views.gotoaddsuperuser),
    # path('get_user_data/',views.get_user_data),
    # # 登录模块


]
