import decimal
import json
import re
from django.contrib.auth import authenticate, login
from django.core.paginator import Paginator
import  time
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from back_app.models import BackAppSystemUser, BackAppRaw,\
    BackAppProvider, BackAppRawHouse, BackAppHouse,BackAppProviderSalesman,BackAppSalesman,BackAppAssignment
from back_app.models_view import SalesmanGrade,ConProvider,ConWarehouser

def mylogin(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')

    if username and password:
        print(1)
        count = BackAppSystemUser.objects.filter(uid=username, password=password).count()
        print(count)
        if count >= 1:
            type1s = BackAppSystemUser.objects.filter(uid=username, password=password)
            for type1 in type1s:
                if type1.utype == 'Sman':
                    return render(request, 'sindex.html')
                elif type1.utype == 'STleader':
                    return render(request, 'tindex.html')
                else:
                    return render(request, 'windex.html')
    return render(request, 'mylogin.html')

    #     user = authenticate(username=username, password=password)
    #     if user is not None:
    #         if user.is_active:
    #             login(request, user)
    #             print(user.is_superuser)
    #             # return HttpResponseRedirect("/",{'username': username})
    #             return render(request, 'mylogin.html', {'username': username})
    #     else:
    #         return render(request, 'mylogin.html', {'mssg': '登录验证失败'})
    # else:
    #     if request.user.is_authenticated:
    #         return render(request, 'mylogin.html', {'username': request.user.username})
    #     else:
    #         return render(request, 'mylogin.html', {'msg': "You should login first!!!!"})

def myregister(request):
    return render(request,'myregister.html')


def table(request):
    return render(request,'table.html')

def get_table(request):
    type1s = BackAppRaw.objects.all()
    lis = []
    for type1 in type1s:
        dic = {}
        dic['rid'] = type1.rid
        dic['rname'] = type1.rname
        dic['rintroduction'] = type1.rintroduction
        lis.append(dic)
    # 前台传来的页数
    print(len(lis))
    page_index = request.GET.get('page')
    # 前台传来的一页显示多少条数据
    page_limit = request.GET.get('limit')
    # 分页器进行分配
    paginator = Paginator(lis, page_limit)
    # 前端传来页数的数据
    data = paginator.page(page_index)
    # 放在一个列表里
    have_paged_info = [x for x in data]
    # students.count()总数据量，layui的table模块要接受的格式
    dict = {"code": 0, "msg": "", "count": type1s.count(), "data": have_paged_info}
    return JsonResponse(dict)



def ajax_get(req):

    return render(req, 'ajax_get.html')

@csrf_exempt
def ajax_post(req):
    if req.method == 'POST':
        print(req.POST)
        print(req.POST.get('name'))
    else:
        print(req.GET)
    return HttpResponse('ajax_post')

def index(rep):
    return render(rep, 'index.html')

def index5(rep):
    return render(rep, 'provider.html')



def get_table_provider(request):
    if request.method == 'GET':
        key_id = request.GET.get('key[pid]')
        if key_id == None:
            type1s = BackAppProvider.objects.all()
            lis = []
            for type1 in type1s:
                dic = {}
                dic['pid'] = type1.pid
                dic['pname'] = type1.pname
                dic['pgrade'] = type1.pgrade
                dic['paddress'] = type1.paddress
                dic['ptelephone'] = type1.ptelephone
                dic['pemail'] = type1.pemail
                lis.append(dic)
            # 前台传来的页数
            print(len(lis))
            page_index = request.GET.get('page')
            # 前台传来的一页显示多少条数据
            page_limit = request.GET.get('limit')
            # 分页器进行分配
            paginator = Paginator(lis, page_limit)
            # 前端传来页数的数据
            data = paginator.page(page_index)
            # 放在一个列表里
            have_paged_info = [x for x in data]
            # students.count()总数据量，layui的table模块要接受的格式
            dict = {"code": 0, "msg": "", "count": type1s.count(), "data": have_paged_info}
            return JsonResponse(dict)
        else:
            type1s = BackAppProvider.objects.filter(pid=key_id)

            lis = []
            for type1 in type1s:
                dic = {}
                dic['pid'] = type1.pid
                dic['pname'] = type1.pname
                dic['pgrade'] = type1.pgrade
                dic['paddress'] = type1.paddress
                dic['ptelephone'] = type1.ptelephone
                dic['pemail'] = type1.pemail
                lis.append(dic)
            # 前台传来的页数
            print(len(lis))
            page_index = request.GET.get('page')
            # 前台传来的一页显示多少条数据
            page_limit = request.GET.get('limit')
            # 分页器进行分配
            paginator = Paginator(lis, page_limit)
            # 前端传来页数的数据
            data = paginator.page(page_index)
            # 放在一个列表里
            have_paged_info = [x for x in data]
            # students.count()总数据量，layui的table模块要接受的格式
            dict = {"code": 0, "msg": "", "count": type1s.count(), "data": have_paged_info}
            return JsonResponse(dict)


@csrf_exempt
def return_table_provider(req):
    if req.method == 'POST':
        print(req.POST)
        print(req.POST.get('clo'))
        print(req.POST.get('val'))
        print(req.POST.get('id'))
        return HttpResponse('yap')



def windex(rep):
    return render(rep, 'windex.html')

def wnumber(rep):
    return render(rep, 'wnumber.html')

def change_s(id):
    s = (str(id))
    p1 = re.compile(r'[(](.*?)[)]', re.S)
    s = re.findall(p1, s)
    return s

def wnumber_get_table(request):
    if request.method == 'GET':
        key_id = request.GET.get('key[rid]')
        if key_id == None:
            type1s = BackAppRawHouse.objects.all()
            lis = []
            for type1 in type1s:
                dic = {}
                dic['rid'] = change_s(type1.rid)
                dic['hid'] = change_s(type1.hid)

                dic['rinventory'] = type1.rinventory

                # dic['renterime'] = str(type1.renterime).split('+')[0]

                lis.append(dic)
            # 前台传来的页数
            page_index = request.GET.get('page')
            # 前台传来的一页显示多少条数据
            page_limit = request.GET.get('limit')
            # 分页器进行分配
            paginator = Paginator(lis, page_limit)
            # 前端传来页数的数据
            data = paginator.page(page_index)
            # 放在一个列表里
            have_paged_info = [x for x in data]
            # students.count()总数据量，layui的table模块要接受的格式
            dict = {"code": 0, "msg": "", "count": type1s.count(), "data": have_paged_info}
            return JsonResponse(dict)


        else:
            type1s = BackAppRawHouse.objects.filter(rid=key_id)
            lis = []
            for type1 in type1s:
                dic = {}
                dic['rid'] = change_s(type1.rid)
                dic['hid'] = change_s(type1.hid)

                dic['rinventory'] = type1.rinventory

                # dic['renterime'] = str(type1.renterime).split('+')[0]

                lis.append(dic)
            # 前台传来的页数
            print(len(lis))
            page_index = request.GET.get('page')
            # 前台传来的一页显示多少条数据
            page_limit = request.GET.get('limit')
            # 分页器进行分配
            paginator = Paginator(lis, page_limit)
            # 前端传来页数的数据
            data = paginator.page(page_index)
            # 放在一个列表里
            have_paged_info = [x for x in data]
            # students.count()总数据量，layui的table模块要接受的格式
            dict = {"code": 0, "msg": "", "count": type1s.count(), "data": have_paged_info}
            return JsonResponse(dict)



@csrf_exempt
def wnumber_return_table(req):
    if req.method == 'POST':
        print(req.POST)
        if str(req.POST.get('op')) == 'ins':
            count = BackAppRawHouse.objects.filter(rid=req.POST.get('id[]')).count()
            if count >= 1:
                val = req.POST.get('val')
                if req.POST.get('clo') == 'rinventory':
                    if float(req.POST.get('val')) <= 0 or float(req.POST.get('val')) > 99999.00:
                        return HttpResponse('No')
                    else:
                        BackAppRawHouse.objects.filter(rid=req.POST.get('id[]')).update(rinventory=val)
                        return HttpResponse('Yes')
                elif BackAppHouse.objects.filter(hid=val).count() < 1:
                        return HttpResponse('No')
                else:
                    BackAppRawHouse.objects.filter(rid=req.POST.get('id[]')).update(hid=val)
                    return HttpResponse('Yes')
            else:
                return HttpResponse('No')
        elif str(req.POST.get('op')) == 'add':
            if BackAppHouse.objects.filter(hid=req.POST.get('hid')).count() < 1 or BackAppRaw.objects.filter(rid=req.POST.get('rid')).count() < 1 or float(req.POST.get('rinventory')) <= 0 or float(req.POST.get('rinventory')) > 99999.00:
                return HttpResponse('No')
            else:
                BackAppRawHouse.objects.create(rid=BackAppRaw(req.POST.get('rid')),
                hid=BackAppHouse(req.POST.get('hid')),rinventory=req.POST.get('rinventory'))
                return HttpResponse('Yes')
        elif str(req.POST.get('op')) == 'del':
            BackAppRawHouse.objects.filter(rid=req.POST.get('rid[]'),hid=req.POST.get('hid[]')).delete()
            return HttpResponse('Yes')
    else:
        print('get')

def wraw(rep):
    return render(rep, 'wraw.html')

def wraw_get_table(request):
    if request.method == 'GET':
        key_id = request.GET.get('key[rid]')
        if key_id == None:
            type1s = BackAppRaw.objects.all()
            lis = []
            for type1 in type1s:
                dic = {}
                dic['rid'] = type1.rid
                dic['rname'] = type1.rname
                dic['rintroduction'] = type1.rintroduction
                # dic['rduetime'] = repr(type1.rduetime)
                # dic['renterime'] = repr(type1.renterime)
                lis.append(dic)
            # 前台传来的页数
            page_index = request.GET.get('page')
            # 前台传来的一页显示多少条数据
            page_limit = request.GET.get('limit')
            # 分页器进行分配
            paginator = Paginator(lis, page_limit)
            # 前端传来页数的数据
            data = paginator.page(page_index)
            # 放在一个列表里
            have_paged_info = [x for x in data]
            # students.count()总数据量，layui的table模块要接受的格式
            dict = {"code": 0, "msg": "", "count": type1s.count(), "data": have_paged_info}
            return JsonResponse(dict)


        else:
            type1s = BackAppRaw.objects.filter(rid=key_id)
            lis = []
            for type1 in type1s:
                dic = {}
                dic['rid'] = type1.rid
                dic['rname'] = type1.rname
                dic['rintroduction'] = type1.rintroduction

                lis.append(dic)
            # 前台传来的页数

            page_index = request.GET.get('page')
            # 前台传来的一页显示多少条数据
            page_limit = request.GET.get('limit')
            # 分页器进行分配
            paginator = Paginator(lis, page_limit)
            # 前端传来页数的数据
            data = paginator.page(page_index)
            # 放在一个列表里
            have_paged_info = [x for x in data]
            # students.count()总数据量，layui的table模块要接受的格式
            dict = {"code": 0, "msg": "", "count": type1s.count(), "data": have_paged_info}
            return JsonResponse(dict)


def whouse(rep):
    return render(rep, 'whouse.html')

def whouse_get_table(request):
    if request.method == 'GET':
        print(request.GET)
        key_id = request.GET.get('key[hid]')
        if key_id == None:
            type1s = BackAppHouse.objects.all()
            lis = []
            for type1 in type1s:
                dic = {}
                dic['hid'] = type1.hid
                dic['hname'] = type1.hname
                dic['hvolume'] = type1.hvolume
                dic['haddress'] = type1.haddress
                # dic['rduetime'] = repr(type1.rduetime)
                # dic['renterime'] = repr(type1.renterime)
                lis.append(dic)
            # 前台传来的页数
            page_index = request.GET.get('page')
            # 前台传来的一页显示多少条数据
            page_limit = request.GET.get('limit')
            # 分页器进行分配
            paginator = Paginator(lis, page_limit)
            # 前端传来页数的数据
            data = paginator.page(page_index)
            # 放在一个列表里
            have_paged_info = [x for x in data]
            # students.count()总数据量，layui的table模块要接受的格式
            dict = {"code": 0, "msg": "", "count": type1s.count(), "data": have_paged_info}
            return JsonResponse(dict)


        else:
            type1s = BackAppHouse.objects.filter(hid=key_id)
            lis = []
            for type1 in type1s:
                dic = {}
                dic['hid'] = type1.hid
                dic['hname'] = type1.hname
                dic['hvolume'] = type1.hvolume
                dic['haddress'] = type1.haddress

                lis.append(dic)
            # 前台传来的页数

            page_index = request.GET.get('page')
            # 前台传来的一页显示多少条数据
            page_limit = request.GET.get('limit')
            # 分页器进行分配
            paginator = Paginator(lis, page_limit)
            # 前端传来页数的数据
            data = paginator.page(page_index)
            # 放在一个列表里
            have_paged_info = [x for x in data]
            # students.count()总数据量，layui的table模块要接受的格式
            dict = {"code": 0, "msg": "", "count": type1s.count(), "data": have_paged_info}
            return JsonResponse(dict)



def whistory(rep):
    return render(rep, 'whistory.html')

def sindex(req):
    return render(req, 'sindex.html')

def sfile(req):
    return render(req, 'sfile.html')


def sfile_get_table(request):
    if request.method == 'GET':
        key_id = request.GET.get('key[pid]')
        if key_id == None:
            type1s = BackAppProviderSalesman.objects.all()
            lis = []
            for type1 in type1s:
                dic = {}
                dic['rid'] = change_s(type1.rid)
                dic['pid'] = change_s(type1.pid)
                dic['sid'] = change_s(type1.sid)
                dic['file'] = type1.file
                dic['rnumber'] = type1.rnumber
                # dic['file'] = repr(type1.file)
                # dic['renterime'] = repr(type1.renterime)
                lis.append(dic)
            # 前台传来的页数
            page_index = request.GET.get('page')
            # 前台传来的一页显示多少条数据
            page_limit = request.GET.get('limit')
            # 分页器进行分配
            paginator = Paginator(lis, page_limit)
            # 前端传来页数的数据
            data = paginator.page(page_index)
            # 放在一个列表里
            have_paged_info = [x for x in data]
            # students.count()总数据量，layui的table模块要接受的格式
            dict = {"code": 0, "msg": "", "count": type1s.count(), "data": have_paged_info}
            return JsonResponse(dict)


        else:
            type1s = BackAppProviderSalesman.objects.filter(pid=key_id)
            lis = []
            for type1 in type1s:
                dic = {}
                dic['rid'] = change_s(type1.rid)
                dic['pid'] = change_s(type1.pid)
                dic['sid'] = change_s(type1.sid)
                dic['file'] = type1.file
                dic['rnumber'] = type1.rnumber

                lis.append(dic)
            # 前台传来的页数

            page_index = request.GET.get('page')
            # 前台传来的一页显示多少条数据
            page_limit = request.GET.get('limit')
            # 分页器进行分配
            paginator = Paginator(lis, page_limit)
            # 前端传来页数的数据
            data = paginator.page(page_index)
            # 放在一个列表里
            have_paged_info = [x for x in data]
            # students.count()总数据量，layui的table模块要接受的格式
            dict = {"code": 0, "msg": "", "count": type1s.count(), "data": have_paged_info}
            return JsonResponse(dict)




@csrf_exempt
def sfile_return_table(req):
    if req.method == 'POST':
        print(str(req.POST))
        if str(req.POST.get('op')) == 'ins':
            count = BackAppProviderSalesman.objects.filter(pid=req.POST.get('pid[]')).count()

            if count >= 1:
                val = req.POST.get('val')
                if req.POST.get('clo') == 'pid':
                    if BackAppProvider.objects.filter(pid=req.POST.get('pid[]')).count() < 1:
                        return HttpResponse('No')
                    else:
                        BackAppProviderSalesman.objects.filter(pid=req.POST.get('pid[]')).update(pid=val)
                        return HttpResponse('Yes')
                elif req.POST.get('clo') == 'sid':
                    if BackAppSalesman.objects.filter(sid=val).count() < 1:
                        return HttpResponse('No')
                    else:
                        BackAppProviderSalesman.objects.filter(pid=req.POST.get('pid[]')).update(sid=val)
                        return HttpResponse('Yes')
                elif req.POST.get('clo') == 'rid':
                    if BackAppRaw.objects.filter(rid=val).count() < 1:
                        return HttpResponse('No')
                    else:
                        BackAppProviderSalesman.objects.filter(pid=req.POST.get('pid[]')).update(rid=val)
                        return HttpResponse('Yes')
                else:
                    if float(val) <= 0 or float(val) > 99999.00:
                        return HttpResponse('No')
                    else:
                        BackAppProviderSalesman.objects.filter(pid=req.POST.get('pid[]')).update(rnumber=val)
                        return HttpResponse('Yes')
            else:
                return HttpResponse('No')
        elif str(req.POST.get('op')) == 'add':
            if BackAppProvider.objects.filter(pid=req.POST.get('pid')).count() < 1 or BackAppRaw.objects.filter(rid=req.POST.get('rid')).count() < 1 or BackAppSalesman.objects.filter(sid=req.POST.get('sid')).count() < 1 or float(req.POST.get('rnumber')) <= 0 or float(req.POST.get('rnumber')) > 99999.00:
                return HttpResponse('No')
            else:
                types1 = ConProvider.objects.filter(pid=req.POST.get('pid'))
                sym = 0
                for type1 in types1:
                    if type1.rid  == str(req.POST.get('rid')):
                        sym = 1
                if sym == 0:
                    return HttpResponse('No')
                BackAppProviderSalesman.objects.create(pid=BackAppProvider(req.POST.get('pid')),
                sid=BackAppSalesman(req.POST.get('sid')),rid=BackAppRaw(req.POST.get('rid')),rnumber=req.POST.get('rnumber'),file ='未完成')
                return HttpResponse('Yes')
        else:
            if BackAppProviderSalesman.objects.filter(pid=req.POST.get('pid[]')).count() < 1:

                return HttpResponse('No')
            else:
                BackAppProviderSalesman.objects.filter(pid=req.POST.get('pid[]')).delete()
                return HttpResponse('Yes')
    else:
        print('get')


def scenter(req):
    return render(req, 'scenter.html')

def stodo(req):
    return render(req, 'stodo.html')

def sprovider(req):
    return render(req, 'sprovider.html')

def sprovider_get_table(request):
    if request.method == 'GET':
        key_id = request.GET.get('key[pid]')
        if key_id == None:

            type1s = ConProvider.objects.all()
            lis = []
            for type1 in type1s:
                dic = {}
                dic['paddress'] = type1.paddress
                dic['pid'] = type1.pid
                dic['ptelephone'] = type1.ptelephone
                dic['pgrade'] = type1.pgrade
                dic['rid'] = type1.rid
                # dic['file'] = repr(type1.file)
                # dic['renterime'] = repr(type1.renterime)
                lis.append(dic)
            # 前台传来的页数
            page_index = request.GET.get('page')
            # 前台传来的一页显示多少条数据
            page_limit = request.GET.get('limit')
            # 分页器进行分配
            paginator = Paginator(lis, page_limit)
            # 前端传来页数的数据
            data = paginator.page(page_index)
            # 放在一个列表里
            have_paged_info = [x for x in data]
            # students.count()总数据量，layui的table模块要接受的格式
            dict = {"code": 0, "msg": "", "count": type1s.count(), "data": have_paged_info}
            return JsonResponse(dict)


        else:
            type1s = ConProvider.objects.filter(pid=key_id)
            lis = []
            print(type1s[0].paddress)


            for type1 in type1s:
                dic = {}
                dic['paddress'] = type1.paddress
                dic['pid'] = type1.pid
                dic['ptelephone'] = type1.ptelephone
                dic['pgrade'] = type1.pgrade
                dic['rid'] = type1.rid
                print(dic)
                lis.append(dic)
            # 前台传来的页数
            page_index = request.GET.get('page')
            # 前台传来的一页显示多少条数据
            page_limit = request.GET.get('limit')
            # 分页器进行分配
            paginator = Paginator(lis, page_limit)
            # 前端传来页数的数据
            data = paginator.page(page_index)
            # 放在一个列表里
            have_paged_info = [x for x in data]
            # students.count()总数据量，layui的table模块要接受的格式
            dict = {"code": 0, "msg": "", "count": type1s.count(), "data": have_paged_info}
            return JsonResponse(dict)







def tindex(req):
    return render(req, 'tindex.html')

def tfile(req):
    return render(req, 'tfile.html')

def tfile_get_table(request):
    if request.method == 'GET':
        key_id = request.GET.get('key[pid]')
        if key_id == None:
            type1s = BackAppProviderSalesman.objects.all()
            lis = []
            for type1 in type1s:
                dic = {}
                dic['rid'] = change_s(type1.rid)
                dic['pid'] = change_s(type1.pid)
                dic['sid'] = change_s(type1.sid)
                dic['file'] = type1.file
                dic['rnumber'] = type1.rnumber
                # dic['file'] = repr(type1.file)
                # dic['renterime'] = repr(type1.renterime)
                lis.append(dic)
            # 前台传来的页数
            page_index = request.GET.get('page')
            # 前台传来的一页显示多少条数据
            page_limit = request.GET.get('limit')
            # 分页器进行分配
            paginator = Paginator(lis, page_limit)
            # 前端传来页数的数据
            data = paginator.page(page_index)
            # 放在一个列表里
            have_paged_info = [x for x in data]
            # students.count()总数据量，layui的table模块要接受的格式
            dict = {"code": 0, "msg": "", "count": type1s.count(), "data": have_paged_info}
            return JsonResponse(dict)


        else:

            type1s = BackAppProviderSalesman.objects.filter(pid=key_id)
            lis = []
            for type1 in type1s:
                dic = {}
                dic['rid'] = change_s(type1.rid)
                dic['pid'] = change_s(type1.pid)
                dic['sid'] = change_s(type1.sid)
                dic['file'] = type1.file
                dic['rnumber'] = type1.rnumber

                lis.append(dic)
            # 前台传来的页数

            page_index = request.GET.get('page')
            # 前台传来的一页显示多少条数据
            page_limit = request.GET.get('limit')
            # 分页器进行分配
            paginator = Paginator(lis, page_limit)
            # 前端传来页数的数据
            data = paginator.page(page_index)
            # 放在一个列表里
            have_paged_info = [x for x in data]
            # students.count()总数据量，layui的table模块要接受的格式
            dict = {"code": 0, "msg": "", "count": type1s.count(), "data": have_paged_info}
            return JsonResponse(dict)


@csrf_exempt
def tfile_return_table(req):
    if req.method == 'POST':
        print(str(req.POST))
        if str(req.POST.get('op')) == 'ins':
            count = BackAppProviderSalesman.objects.filter(rid=req.POST.get('rid[]'),sid=req.POST.get('sid[]')).count()
            if count >= 1:
                val = req.POST.get('val')
                if val == '未完成' or val == '已完成':
                    BackAppProviderSalesman.objects.filter(rid=req.POST.get('rid[]'),sid=req.POST.get('sid[]')).update(file=val)
                    return HttpResponse('Yes')
                else:return HttpResponse('No')
            else:
                return HttpResponse('No')
        else:
            if BackAppProviderSalesman.objects.filter(rid=req.POST.get('rid[]'),sid=req.POST.get('sid[]')).count() < 1:
                return HttpResponse('No')
            else:
                BackAppProviderSalesman.objects.filter(rid=req.POST.get('rid[]'),sid=req.POST.get('sid[]')).delete()
                return HttpResponse('Yes')
    else:
        print('get')

def tinventory(req):
    return render(req, 'tinventory.html')


def tinventory_get_table(request):
    if request.method == 'GET':
        key_id = request.GET.get('key[rid]')
        if key_id == None:
            type1s = BackAppAssignment.objects.all()
            lis = []
            for type1 in type1s:
                dic = {}
                dic['rid'] = change_s(type1.rid)
                dic['rname'] = type1.rname
                dic['rnumber'] = type1.rnumber
                dic['sid'] = change_s(type1.sid)

                # dic['file'] = repr(type1.file)
                # dic['renterime'] = repr(type1.renterime)
                lis.append(dic)
            # 前台传来的页数
            page_index = request.GET.get('page')
            # 前台传来的一页显示多少条数据
            page_limit = request.GET.get('limit')
            # 分页器进行分配
            paginator = Paginator(lis, page_limit)
            # 前端传来页数的数据
            data = paginator.page(page_index)
            # 放在一个列表里
            have_paged_info = [x for x in data]
            # students.count()总数据量，layui的table模块要接受的格式
            dict = {"code": 0, "msg": "", "count": type1s.count(), "data": have_paged_info}
            return JsonResponse(dict)


        else:

            type1s = BackAppAssignment.objects.filter(rid=key_id)
            lis = []
            for type1 in type1s:
                dic = {}
                dic['rid'] = change_s(type1.rid)
                dic['rname'] = type1.rname
                dic['rnumber'] = type1.rnumber
                dic['sid'] = change_s(type1.sid)
                lis.append(dic)
            # 前台传来的页数

            page_index = request.GET.get('page')
            # 前台传来的一页显示多少条数据
            page_limit = request.GET.get('limit')
            # 分页器进行分配
            paginator = Paginator(lis, page_limit)
            # 前端传来页数的数据
            data = paginator.page(page_index)
            # 放在一个列表里
            have_paged_info = [x for x in data]
            # students.count()总数据量，layui的table模块要接受的格式
            dict = {"code": 0, "msg": "", "count": type1s.count(), "data": have_paged_info}
            return JsonResponse(dict)


@csrf_exempt
def tinventory_return_table(req):
    if req.method == 'POST':
        print(str(req.POST))
        if str(req.POST.get('op')) == 'ins':
            count = BackAppAssignment.objects.filter(rid=req.POST.get('rid[]')).count()
            if count >= 1:
                if BackAppSalesman.objects.filter(sid=req.POST.get('val')).count() < 1:
                    return HttpResponse('No')
                else:

                    BackAppAssignment.objects.filter(rid=req.POST.get('rid[]')).update(sid=req.POST.get('val'))
                    return HttpResponse('Yes')
            else:
                return HttpResponse('No')
        elif str(req.POST.get('op')) == 'add':
            if BackAppRaw.objects.filter(rid=req.POST.get('rid')).count() < 1 or float(req.POST.get('number')) <= 0 or float(req.POST.get('number')) > 99999.00:
                return HttpResponse('No')
            else:
                types1 = BackAppRaw.objects.filter(rid=req.POST.get('rid'))
                for type1 in types1:
                    if type1.rname != str(req.POST.get('rname')):
                        # print(type1.rname)
                        return HttpResponse('No')
                now_number = float(req.POST.get('number'))
                kucun_number = 0.0
                types1 = BackAppRawHouse.objects.filter(rid = req.POST.get('rid'))
                for type1 in types1:
                    kucun_number = float(type1.rinventory) - now_number
                    now_number = now_number - float(type1.rinventory) # 判断是否库存有

                if kucun_number <= 0:
                    kucun_number = 0
                if now_number <= 0:
                    BackAppRawHouse.objects.filter(rid=req.POST.get('rid')).update(rinventory = kucun_number)
                    return HttpResponse('Yes')
                BackAppRawHouse.objects.filter(rid=req.POST.get('rid')).update(rinventory=kucun_number)
                BackAppAssignment.objects.create(rname=req.POST.get('rname'),rid=BackAppRaw(req.POST.get('rid')),rnumber=now_number)
                return HttpResponse('Yes')
        elif str(req.POST.get('op')) == 'del':
            if BackAppAssignment.objects.filter(rid=req.POST.get('rid[]')).count() < 1:
                return HttpResponse('No')
            else:
                BackAppAssignment.objects.filter(rid=req.POST.get('rid[]')).delete()
                return HttpResponse('Yes')
    else:
        print('get')

def tsale(req):
    return render(req, 'tsale.html')

def tsale_get_table(request):
    if request.method == 'GET':
        key_id = request.GET.get('key[sid]')
        if key_id == None:
            type1s = BackAppSalesman.objects.all()
            lis = []
            for type1 in type1s:
                dic = {}
                dic['sname'] = type1.sname
                dic['sname'] = type1.sname
                dic['sid'] = type1.sid
                dic['sgender'] = type1.sgender
                dic['sgrade'] = type1.sgrade
                dic['stelephone'] = type1.stelephone
                dic['semail'] = type1.semail
                # dic['file'] = repr(type1.file)
                # dic['renterime'] = repr(type1.renterime)
                lis.append(dic)
            # 前台传来的页数
            page_index = request.GET.get('page')
            # 前台传来的一页显示多少条数据
            page_limit = request.GET.get('limit')
            # 分页器进行分配
            paginator = Paginator(lis, page_limit)
            # 前端传来页数的数据
            data = paginator.page(page_index)
            # 放在一个列表里
            have_paged_info = [x for x in data]
            # students.count()总数据量，layui的table模块要接受的格式
            dict = {"code": 0, "msg": "", "count": type1s.count(), "data": have_paged_info}
            return JsonResponse(dict)


        else:

            type1s = BackAppSalesman.objects.filter(sid=key_id)
            lis = []
            for type1 in type1s:
                dic = {}
                dic['sname'] = type1.sname
                dic['sname'] = type1.sname
                dic['sid'] = type1.sid
                dic['sgender'] = type1.sgender
                dic['sgrade'] = type1.sgrade
                dic['stelephone'] = type1.stelephone
                dic['semail'] = type1.semail

                lis.append(dic)
            # 前台传来的页数

            page_index = request.GET.get('page')
            # 前台传来的一页显示多少条数据
            page_limit = request.GET.get('limit')
            # 分页器进行分配
            paginator = Paginator(lis, page_limit)
            # 前端传来页数的数据
            data = paginator.page(page_index)
            # 放在一个列表里
            have_paged_info = [x for x in data]
            # students.count()总数据量，layui的table模块要接受的格式
            dict = {"code": 0, "msg": "", "count": type1s.count(), "data": have_paged_info}
            return JsonResponse(dict)


def traw(req):
    return render(req, 'traw.html')