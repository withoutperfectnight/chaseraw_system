# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class SalesmanGrade(models.Model):
    sid = models.CharField(primary_key= True,db_column='Sid', max_length=10,
                           db_collation='utf8mb4_0900_ai_ci')  # Field name made lowercase.
    sname = models.CharField(db_column='Sname', max_length=20,
                             db_collation='utf8mb4_0900_ai_ci')  # Field name made lowercase.
    sgrade = models.DecimalField(db_column='Sgrade', max_digits=5, decimal_places=2)  # Field name made lowercase.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'salesman_grade'


class ConProvider(models.Model):
    pid = models.CharField(db_column='Pid', max_length=4,
                           db_collation='utf8mb4_0900_ai_ci')  # Field name made lowercase.
    rid = models.CharField(primary_key= True,db_column='RId', max_length=10,
                           db_collation='utf8mb4_0900_ai_ci')  # Field name made lowercase.
    paddress = models.CharField(db_column='Paddress', max_length=200,
                                db_collation='utf8mb4_0900_ai_ci')  # Field name made lowercase.
    ptelephone = models.CharField(db_column='Ptelephone', max_length=11,
                                  db_collation='utf8mb4_0900_ai_ci')  # Field name made lowercase.
    pgrade = models.DecimalField(db_column='Pgrade', max_digits=5, decimal_places=2)  # Field name made lowercase.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'con_provider'


class ConWarehouser(models.Model):
    hid = models.CharField(primary_key= True,db_column='Hid', max_length=4,
                           db_collation='utf8mb4_0900_ai_ci')  # Field name made lowercase.
    hname = models.CharField(db_column='Hname', max_length=20,
                             db_collation='utf8mb4_0900_ai_ci')  # Field name made lowercase.
    rid = models.CharField(db_column='Rid', max_length=10,
                           db_collation='utf8mb4_0900_ai_ci')  # Field name made lowercase.
    rinventory = models.DecimalField(db_column='Rinventory', max_digits=7,
                                     decimal_places=2)  # Field name made lowercase.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'con_warehouser'