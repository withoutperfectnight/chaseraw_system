# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class BackAppAssignment(models.Model):
    rid = models.OneToOneField('BackAppRaw', models.DO_NOTHING, db_column='Rid', primary_key=True)  # Field name made lowercase.
    rname = models.CharField(db_column='Rname', max_length=20)  # Field name made lowercase.
    rnumber = models.DecimalField(db_column='Rnumber', max_digits=7, decimal_places=2)  # Field name made lowercase.
    sid = models.ForeignKey('BackAppSalesman', models.DO_NOTHING, db_column='Sid', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'back_app_assignment'


class BackAppHouse(models.Model):
    hid = models.CharField(db_column='Hid', primary_key=True, max_length=4)  # Field name made lowercase.
    hname = models.CharField(db_column='Hname', max_length=20)  # Field name made lowercase.
    hvolume = models.DecimalField(db_column='Hvolume', max_digits=7, decimal_places=2)  # Field name made lowercase.
    haddress = models.CharField(db_column='Haddress', max_length=200)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'back_app_house'


class BackAppProvider(models.Model):
    pid = models.CharField(db_column='Pid', primary_key=True, max_length=4)  # Field name made lowercase.
    pname = models.CharField(db_column='Pname', max_length=50)  # Field name made lowercase.
    pgrade = models.DecimalField(db_column='Pgrade', max_digits=5, decimal_places=2)  # Field name made lowercase.
    paddress = models.CharField(db_column='Paddress', max_length=200)  # Field name made lowercase.
    ptelephone = models.CharField(db_column='Ptelephone', max_length=11)  # Field name made lowercase.
    pemail = models.CharField(db_column='Pemail', max_length=20)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'back_app_provider'


class BackAppProviderRaw(models.Model):
    pid = models.OneToOneField(BackAppProvider, models.DO_NOTHING, db_column='Pid', primary_key=True)  # Field name made lowercase.
    rid = models.ForeignKey('BackAppRaw', models.DO_NOTHING, db_column='RId')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'back_app_provider_raw'


class BackAppProviderSalesman(models.Model):
    pid = models.OneToOneField(BackAppProvider, models.DO_NOTHING, db_column='Pid', primary_key=True)  # Field name made lowercase.
    sid = models.ForeignKey('BackAppSalesman', models.DO_NOTHING, db_column='Sid')  # Field name made lowercase.
    rid = models.ForeignKey('BackAppRaw', models.DO_NOTHING, db_column='Rid')  # Field name made lowercase.
    rnumber = models.DecimalField(db_column='Rnumber', max_digits=7, decimal_places=2)  # Field name made lowercase.
    file = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'back_app_provider_salesman'
        unique_together = (('pid', 'sid', 'rid'),)


class BackAppRaw(models.Model):
    rid = models.CharField(db_column='Rid', primary_key=True, max_length=10)  # Field name made lowercase.
    rname = models.CharField(db_column='Rname', max_length=20)  # Field name made lowercase.
    rintroduction = models.CharField(db_column='Rintroduction', max_length=200)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'back_app_raw'


class BackAppRawHouse(models.Model):
    rid = models.OneToOneField(BackAppRaw, models.DO_NOTHING, db_column='Rid', primary_key=True)  # Field name made lowercase.
    hid = models.ForeignKey(BackAppHouse, models.DO_NOTHING, db_column='Hid')  # Field name made lowercase.
    rinventory = models.DecimalField(db_column='Rinventory', max_digits=7, decimal_places=2)  # Field name made lowercase.
    renterime = models.DateTimeField(db_column='Renterime', blank=True, null=True)  # Field name made lowercase.
    rduetime = models.DateTimeField(db_column='Rduetime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'back_app_raw_house'
        unique_together = (('rid', 'hid'),)


class BackAppSalesman(models.Model):
    sid = models.CharField(db_column='Sid', primary_key=True, max_length=10)  # Field name made lowercase.
    sname = models.CharField(db_column='Sname', max_length=20)  # Field name made lowercase.
    sage = models.IntegerField(db_column='Sage')  # Field name made lowercase.
    sgender = models.CharField(db_column='Sgender', max_length=20)  # Field name made lowercase.
    sgrade = models.DecimalField(db_column='Sgrade', max_digits=5, decimal_places=2)  # Field name made lowercase.
    stelephone = models.CharField(db_column='Stelephone', max_length=11)  # Field name made lowercase.
    semail = models.CharField(db_column='Semail', max_length=20)  # Field name made lowercase.
    sintroduction = models.CharField(db_column='Sintroduction', max_length=400)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'back_app_salesman'


class BackAppSalesmanLeader(models.Model):
    tid = models.CharField(db_column='Tid', primary_key=True, max_length=10)  # Field name made lowercase.
    tname = models.CharField(db_column='Tname', max_length=20)  # Field name made lowercase.
    tage = models.IntegerField(db_column='Tage')  # Field name made lowercase.
    tgender = models.CharField(db_column='Tgender', max_length=20)  # Field name made lowercase.
    ttelephone = models.CharField(db_column='Ttelephone', max_length=11)  # Field name made lowercase.
    temail = models.CharField(db_column='Temail', max_length=20)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'back_app_salesman_leader'


class BackAppSystemUser(models.Model):
    uid = models.CharField(db_column='Uid', primary_key=True, max_length=10)  # Field name made lowercase.
    uname = models.CharField(db_column='Uname', max_length=20)  # Field name made lowercase.
    password = models.CharField(max_length=128)
    ugender = models.CharField(db_column='Ugender', max_length=20)  # Field name made lowercase.
    uemail = models.CharField(db_column='Uemail', max_length=20)  # Field name made lowercase.
    utelephone = models.CharField(db_column='Utelephone', max_length=11)  # Field name made lowercase.
    utype = models.CharField(db_column='Utype', max_length=20)  # Field name made lowercase.
    uvisible = models.IntegerField(db_column='Uvisible')  # Field name made lowercase.
    ucreatetime = models.DateTimeField(db_column='UcreateTime')  # Field name made lowercase.
    last_login = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'back_app_system_user'


class BackAppWarehouser(models.Model):
    wid = models.CharField(db_column='Wid', primary_key=True, max_length=10)  # Field name made lowercase.
    wname = models.CharField(db_column='Wname', max_length=20)  # Field name made lowercase.
    wgender = models.CharField(db_column='Wgender', max_length=20)  # Field name made lowercase.
    wtelephone = models.CharField(db_column='Wtelephone', max_length=11)  # Field name made lowercase.
    wemail = models.CharField(db_column='Wemail', max_length=20)  # Field name made lowercase.
    hid = models.ForeignKey(BackAppHouse, models.DO_NOTHING, db_column='Hid')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'back_app_warehouser'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'
